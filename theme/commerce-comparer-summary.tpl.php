<div class="compare-summary-wrapper">
  <div class="remove"><?php print $remove_icon; ?></div>
  <div class="content"><?php print drupal_render_children($content); ?></div>
  <div class="title">
    <a href="<?php print $display_url; ?>"><?php print $product_title; ?></a>
  </div>
</div>